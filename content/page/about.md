---
title: "About"
description: "I am a Computational Scientist currently employed at Virginia Tech. As an engineer at heart, curiosity is my greatest gift and here I have attempted to summarize my ever-growing list of interests."
featured_image: ''
---
<!---#{{< figure src="/images/Victor_Hugo-Hunchback.jpg" title="At the great sand dunes in colorado" >}}--->
<h2 class="mb-5">A little about me...</h2>

<p class="mb-5">Hey there! I am currently employed at Virginia Tech as a Computational Scientist since September 2014. My research interests lie in Numerical Methods, Computational Science and Machine Learning using High-Performance Computing with traditional and novel accelerator technologies. Since joining VT, I have also had a chance to hone my skills at Data Visualization. My background during my Doctoral work at the SimCenter:National Center for Computational Engineering was in CFD and Electromagnetics using the Finite-Element Method.</p>
          <p class="mb-5">
          I have a Doctorate in Computational Engineering from the University of Tennessee and a Masters in Electrical Engineering from the The Pennsylvania State univiersity.
          </p>

<section class="resume-section p-3 p-lg-5 d-flex flex-column" id="education">
        <div class="my-auto">
          <h2 class="mb-5">Teaching</h2>

	 <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Introduction to Python, CS1064 </h3>
              <div class="subheading mb-3"></div>
              <p>This class was an introductory class for non-Computer Science majors to be introduced to the basics of programming concepts using Python.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Spring 2016</span>
            </div>
          </div>

	 <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Introduction to OpenACC, CMDA 3634: Comp Sci Foundations for CMDA</h3>
              <div class="subheading mb-3"></div>
              <p>Lectured on the OpenACC framework for GPU computing in Prof. Tim Warburton's class 'Foundations for Computational Modelling'. This framework simplifies GPU computing by using directive-based acceleration, instead of the explicit parallelization required by CUDA.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Spring 2015</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Hands-on Workshop for the Industrial  and Systems Engineering (ISE) Department</h3>
              <div class="subheading mb-3"></div>
              <p> <ul> <li> Introduction to Python for Scientific Computing - This introduced the basics of Python as well as numerical libraries such as NumPy and SciPy tailored to the ISE department. This class also introduced students to the JupyterHub framework for collaborative computing and visualization.
</li> <li> Introduction to Data Visualization with Plotly - This class introduced the Data Visualization framework for exploratory visualization and presenting data relevant to the ISE department.</li> </ul> </p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Spring 2017</span>
            </div>
          </div>

	<div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Introduction to Scientific Computing using Python</h3>
              <div class="subheading mb-3"></div>
              <p>This introduced the basics of Python as well as numerical libraries such as NumPy, Scipy, Debugging, Interfacing with C, plotting libraries Matplotlib.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Every Semester</span>
            </div>
          </div>

          <div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Introduction to Data Visualization</h3>
              <div class="subheading mb-3"></div>
              <p>Introduced the basics of Visualization techniques and also taught users how to use the visualization package Plotly for two-dimensional and three-dimensional information and scientific visualization. At the end of this session users should be able to understand and generate interactive bar charts, line charts, scatterplots, choropleths, proportional symbol maps, 3D topographic maps, network charts to name a few.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Every Semester</span>
            </div>
          </div>

	<div class="resume-item d-flex flex-column flex-md-row">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">Introduction to CUDA</h3>
              <div class="subheading mb-3"></div>
              <p>CUDA is a parallel programming paradigm used for GPUs which can provide massively parallel execution of high-performance codes. Almost all major supercomputers are now equipped with GPUs and they have played a pivotal role in the last decade for expediting many scientific discoveries. This course introduces participants to the GPU architecture and the programming paradigm CUDA.</p>
            </div>
            <div class="resume-date text-md-right">
              <span class="text-primary">Spring 2015</span>
            </div>
          </div>


        </div>
      </section>


</section class="resume-section p-3 p-lg-5 d-flex flex-column" id="awards">
        <div class="my-auto">
          <h2 class="mb-5">Positions Held</h2>
          <ul class="fa-ul mb-0">
            <li>
              <i class="fa-li fa fa-trophy text-warning"></i>
              XSEDE Campus Champion</li>
            <li>
              <i class="fa-li fa fa-trophy text-warning"></i>
              ACI-REF Campus Representative</li>
            <li>
              <i class="fa-li fa fa-trophy text-warning"></i>
              OpenACC Campus Representative</li>
            <li>
              <i class="fa-li fa fa-trophy text-warning"></i>
              XSEDE Conference Session Chair and Paper Reviewer</li>
            <li>
              <i class="fa-li fa fa-trophy text-warning"></i>
              NSF Proposal Reviewer</li>
          </ul>
        </div>
      </section>
