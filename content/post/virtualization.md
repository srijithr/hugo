---
title: "Virtualization technology for Research Computing"
subtitle: "Virtual Machines, system and application containers"
date: 2018-01-11
tags: ["Openstack","Docker"]
---
<h2 class="mb-5">Virtualization Technology</h2>

Some of the  virtualization technologies that span Cloud Computing services, listed in increasing order of resource usage, is listed below. This list is not comprehensive, simply an overview of the popular options:

<h3>Application Containers (e.g. Docker/Rocket)</h3>
Open platform for running applications in a linux container. This is usually used to run a single process per container and workloads requiring multiple processes are encouraged to be run in different containers that ‘talk’ to each other. While this may be the docker philosophy, there are plenty of deployments where this is not the case. 

Docker(https://www.docker.com/) has wide adoption and Docker Hub(https://hub.docker.com/) has a lot of growing number of application images. Supported on almost all Enterprise clouds such as AWS and Microsoft Azure. 

<h3>System Containers ( e.g. LXC/Singularity )</h3>
 Open platforms for running full workflows inside a linux container. This differs from application containers in the sense that an entire linux environment is set up here, whereas application containers treat filesystems and networks as abstract. Application containers can be deployed from inside the system container. LXC (https://linuxcontainers.org/) is mature and Canonical packages LXC into their latest Ubuntu OS versions through LXD (https://linuxcontainers.org/lxd/introduction/) which makes managing LXD containers easier. LXC offers a number of linux distros that be used to set up an environment. Singularity is conceptually similar to LXC, however it also provides the ability to run Docker containers. It is also aimed at the traditional HPC environment for running jobs that span multiple nodes through a parallel programming paradigm such as MPI.

<h3>OpenStack</h3>
OpenStack (https://www.openstack.org/software/) is a set of software tools for deploying and managing Virtual Machines on the cloud and falls strictly into the ‘Infrastructure as a Service’ category. Unlike Containers which provide OS-level virtualization through self-contained userspace, VMs emulate physical hardware through Hypervisor technology. This provides a great level of isolation for guest environments and support a larger number of users, however because of the fact that a separate kernel space needs to be spawned for each VM it tends to have a greater memory footprint. Also, this has the ability to run any OS unlike containers which are only for linux environments.

