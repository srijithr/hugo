## About  

I am a Computational Scientist currently employed at Virginia Tech. As an engineer 
at heart, curiosity is my greatest gift and here I have attempted to summarize my 
ever-growing list of interests.

